import {dummyCars} from './cars';

export const dummyProspects = [
  {
    id: '1',
    name: 'Alvis Ryan',
    car: dummyCars[0],
    birthDate: '1990-04-10',
  },
  {
    id: '2',
    name: 'Roxanne Gibson',
    car: dummyCars[1],
    birthDate: '1990-04-10',
  },
  {
    id: '3',
    name: 'Halie Howell',
    car: dummyCars[2],
    birthDate: '1990-04-10',
  },
];
