import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,
  StyleSheet,
  Button,
  Alert,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {dummyProspects} from '../dummyDatas/prospects';
import {dummyCars} from '../dummyDatas/cars';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import MapView from 'react-native-maps';

const {width, height} = Dimensions.get('window');

const DefaultInput = ({label, value, onChangeText, children}) => {
  return (
    <View style={[styles.row, styles.flexCenter]}>
      <View style={{width: width * 0.2}}>
        <Text style={{textAlign: 'left'}}>{label}</Text>
      </View>
      {!children && (
        <View
          style={{
            width: width * 0.6,
            borderBottomWidth: 1,
            borderBottomColor: '#ddd',
          }}>
          <TextInput value={value} onChangeText={(v) => onChangeText(v)} />
        </View>
      )}
      {children && children}
    </View>
  );
};

const AddProspect = (props) => {
  const mappedCarList = dummyCars.map((car) => ({
    value: car,
    label: car.name,
  }));

  const [selectedCar, setSelectedCar] = useState(mappedCarList[0]);
  const [prospectName, setProspectName] = useState('');
  const [birthday, setBirthday] = useState('');
  const [avatar, setAvatar] = useState(null);

  const [requestStatus, setRequestStatus] = useState('initial');

  const API_URL = 'https://5f1500684693a6001627506f.mockapi.io/api/v1/prospects';

  const handleSubmit = props => {
    const requestBody = {
      name: prospectName,
      birthDate: birthday,
      car: selectedCar.value,
      avatar,
    };

    setRequestStatus('pending');

    axios
      .post(API_URL, requestBody)
      .then((_) => {
        Alert.alert('Save Succeeded');
        setRequestStatus('succeeded');
      })
      .catch((_) => {
        Alert.alert('Save Failed');
        setRequestStatus('failed');
      });
  }

  const handlePickImage = () => {
    const options = {
      title: 'Select Picture',
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('response from img picker', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = {uri: response.uri};

        // You can also display the image using data:
        const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setAvatar(source);
      }
    });
  };

  return (
    <View style={{flex: 1, backgroundColor: '#FFF'}}>
      <View>
        <TouchableOpacity onPress={handlePickImage}>
          <Image
            source={avatar ? avatar : require("../assets/images/default_photo.png")}
            style={{width, height: 200}}
            resizeMode={'contain'}
          />
        </TouchableOpacity>
      </View>
      <View style={{margin: 10, backgroundColor: 'grey', alignSelf: 'center'}}>
        <MapView
          style={{height: 200, width: 300, margin: 10}}
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
      </View>
      <View>
        <DefaultInput label="Car">
          <Picker
            style={{width: '60%', height: 50}}
            selectedValue={selectedCar.value}
            onValueChange={(value) =>
              setSelectedCar(
                mappedCarList.find((car) => car.value.id === value.id),
              )
            }>
            {mappedCarList.map((option) => (
              <Picker.Item
                label={option.label}
                value={option.value}
                key={option.value.id}
              />
            ))}
          </Picker>
        </DefaultInput>
        <DefaultInput label="Name" onChangeText={setProspectName} />
        <DefaultInput label="BirthDate" onChangeText={setBirthday} />
        <View style={{alignItems: 'center', marginTop: 30}}>
          <Button
            style={{width: 100}}
            title={requestStatus === 'pending' ? 'Loading...' : 'SAVE'}
            onPress={handleSubmit}
          />
        </View>
      </View>
    </View>
  );
};

export default AddProspect;

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  flexCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  image: {
    width,
    height: 200,
  },
});
